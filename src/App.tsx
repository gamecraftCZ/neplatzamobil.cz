import React from "react";
import { Disclaimer } from "./components/Disclaimer";
import "./App.sass";
import { Application } from "./components/Application";

export default function App() {
  console.log("ahoj");
  return (
    <div className="App">
      <Application />
      <Disclaimer />
    </div>
  );
}
