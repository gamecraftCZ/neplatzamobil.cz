import React, { ReactElement } from "react";
import { numberPatternMapping, PRICING } from "./numberPatternMatching";

export const useNumberInfo = (phoneNumber: string): ReactElement => {
  // Trim spaces
  phoneNumber = phoneNumber.replace(/[^0-9*#]/g, "");

  // Test for non czech number
  if (!/^420/.test(phoneNumber) && phoneNumber.length > 9) {
    return (
      <>
        Telefonní číslo: {phoneNumber} <br />
        <div className="warning">Pozor! Tohle nevypadá jako české číslo!</div>
      </>
    );
  }

  // Remove 420 on the end of phone number
  if (phoneNumber.length > 9) {
    phoneNumber = phoneNumber.replace(/^420/, "");
  }
  console.log("Number_Parsed: ", phoneNumber);

  // Test pattern matching
  for (let pattern of numberPatternMapping) {
    if (pattern.pattern.test(phoneNumber)) {
      const result = pattern.result(phoneNumber);
      return (
        <>
          Telefonní číslo: {phoneNumber} <br />
          {getPriceElement(result.price, result.priceCZK)} <br />
          {result.message}
        </>
      );
    }
  }

  // Phone number looks ok
  return (
    <>
      Telefonní číslo: {phoneNumber} <br />
      Toto telefonní číslo nevypadá speciálně.
    </>
  );
};

function getPriceElement(price: PRICING, priceCZK?: number) {
  switch (price) {
    case PRICING.FREE:
      return <div className="free">Volání na toto číslo je zadarmo.</div>;
    case PRICING.NORMAL:
      return <div className="normal">Volání na toto číslo je za normální cenu.</div>;
    case PRICING.NORMAL_PAID:
      return <div className="normal-payed">POZOR! Na toto číslo se nevztahují volné minuty!</div>;
    case PRICING.PAID_PER_MINUTE:
      return (
        <div className="payed">POZOR! Volání na toto číslo je drahé! ({priceCZK} Kč / minuty)</div>
      );
    case PRICING.PAID_PER_CALL:
      return (
        <div className="payed">
          POZOR! Volání na toto číslo je drahé! ({priceCZK} Kč / uskutečněný hovor)
        </div>
      );
  }
}
