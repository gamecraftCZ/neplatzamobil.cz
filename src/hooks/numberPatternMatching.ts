export enum PRICING {
  FREE, // Free
  NORMAL, // Normal price
  NORMAL_PAID, // Normal price, but not covered by a phone plan
  PAID_PER_MINUTE, // Special pricing, per minute
  PAID_PER_CALL, // Special pricing, per call
}
export interface MatchResult {
  price: PRICING;
  priceCZK?: number; // -1 = dle tarifu operátora
  message: string;
}
export interface PatternElementMap {
  pattern: RegExp;
  result: (text: string) => MatchResult;
}

// Zdroje patternů:
// https://www.ctu.cz/cs/download/ochrana_spotrebitele/ochrana_spotrebitele_informace-ucastnici_cislo-9.pdf
// https://www.ctu.cz/sites/default/files/cs/download/ochrana_spotrebitele/barevne-linky.pdf
export const numberPatternMapping: PatternElementMap[] = [
  // region Tísňové linky
  {
    pattern: /^112$/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Mezinárodní tísňová linka.",
      };
    },
  },
  {
    pattern: /^150$/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Hasiči.",
      };
    },
  },
  {
    pattern: /^155$/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Záchranná služba.",
      };
    },
  },
  {
    pattern: /^156$/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Městská policie.",
      };
    },
  },
  {
    pattern: /^158$/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Státní policie.",
      };
    },
  },
  // endregion
  // region Numbers 9xx - Placené služby
  {
    pattern: /^910|^971/,
    result: text => {
      return {
        price: PRICING.NORMAL,
        priceCZK: -1,
        message: "Číslo je používané jako přístupový kód k sítím elektronických komunikací.",
      };
    },
  },
  {
    pattern: /^93|^969|^970|^977/,
    result: text => {
      return {
        price: PRICING.NORMAL,
        priceCZK: -1,
        message: "Číslo je používané jako směrovací kód k veřejným komunikačním sítím a službám.",
      };
    },
  },
  {
    pattern: /^972|^973|^974|^980|^983|^95|^989/,
    result: text => {
      return {
        price: PRICING.NORMAL,
        priceCZK: -1,
        message: "Číslo je používané jako směrovací kód k neveřejným komunikačním sítím a službám.",
      };
    },
  },
  {
    pattern: /^900|^906/,
    result: text => {
      return {
        price: PRICING.PAID_PER_MINUTE,
        priceCZK: Number.parseInt(text.slice(3, 5)),
        message: "Obchodní odborné, inzertní a soutěžní služby.",
      };
    },
  },
  {
    pattern: /^909/,
    result: text => {
      return {
        price: PRICING.PAID_PER_MINUTE,
        priceCZK: Number.parseInt(text.slice(3, 5)),
        message: "Zábavné služby pro dospělé.",
      };
    },
  },
  {
    pattern: /^908/,
    result: text => {
      return {
        price: PRICING.PAID_PER_CALL,
        priceCZK: Number.parseInt(text.slice(3, 5)),
        message: "Číslo placené za zavolání.",
      };
    },
  },
  {
    pattern: /^905/,
    result: text => {
      return {
        price: PRICING.PAID_PER_CALL,
        priceCZK: Number.parseInt(text.slice(3, 5)) * 10,
        message: "Číslo placené za zavolání.",
      };
    },
  },
  {
    pattern: /^976/,
    result: text => {
      return {
        price: PRICING.PAID_PER_MINUTE,
        priceCZK: Number.parseInt(text.slice(3, 5)) * 100,
        message: "Číslo užívané k přístupu k internetu. Dnes již skoro nepoužívané.",
      };
    },
  },
  // endregion
  // region Numbers 8xx - Barevné linky
  {
    pattern: /^800/,
    result: text => {
      return {
        price: PRICING.FREE,
        message: "Zelená linka. Volání je zdarma.",
      };
    },
  },
  {
    pattern: /^81|^83|^843|^844|^845|^846/,
    result: text => {
      return {
        price: PRICING.NORMAL_PAID,
        message:
          "Modrá linka. Cena volání je snížená, dle speciálného ceníku operátora.",
      };
    },
  },
  {
    pattern: /^840|^841|^842|^847|^848|^849/,
    result: text => {
      return {
        price: PRICING.NORMAL_PAID,
        message:
          "Bílá linka. Cena volání dle ceníku operátora.",
      };
    },
  },
  // endregion
];
