import React, { useState } from "react";

export const Disclaimer = () => {
  const [opened, setOpened] = useState(false);

  return (
    <div className="disclaimer">
      {opened ? (
        <div className="content">
          Použití tohoto webu je na vlastní riziko. Nezodpovídám za škody vzniklé chybami, špatnou
          funkčností, či jinými problémy, které se mohou vyskytnout.
        </div>
      ) : null}
      <button onClick={() => setOpened(!opened)}>!</button>
    </div>
  );
};
