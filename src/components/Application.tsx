import React, { useState } from "react";
import { useNumberInfo } from "../hooks/useNumberInfo";

export const Application = () => {
  const [phone, setPhone] = useState("");
  const info = useNumberInfo(phone);

  return (
    <div className="application">
      <div>Neplať za předražené linky! Zkontroluj číslo zde.</div>
      <div>
        <input
          type="text"
          inputMode="tel"
          value={phone}
          onChange={e => setPhone(e.target.value)}
          placeholder="901 998 877"
        />
      </div>
      <div className="number-info">{info}</div>
    </div>
  );
};
