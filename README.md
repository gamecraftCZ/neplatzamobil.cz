# NeplaťZaMobil.cz
NeplaťZaMobil je jednoduchá webová aplikace, která pomáhá lidem identifikovat speciální placená čísla. 
 
Že udělám takovouto aplikace jsem se rozhodl po zjištění, že mi byl zaúčtován hovor na podporu českého Microsoftu (pak se ještě v Microsoftu diví, proč je nemá nikdo rád).
V mém případě to bylo jen pár desítek korun, ale jsou i případy, kdy to může způsobit škody v řádech tisíců.  
Pokud díky NeplaťZaMobil zabráním byť jen jednomu takovému případu, tak to bude velký úspěch.

#### This project is licensed under the terms of the MIT license.